var express = require('express');
var router = express.Router();
var request = require('request');
var atCredentials = {
  apiKey: process.env.AT_APIKEY,
  username: process.env.AT_USERNAME,
};
var CREATESUBSCRIPTIONURL = 'https://api.africastalking.com/version1/subscription/create';
var PREMIUMSHORTCODE = '22384';
var PREMIUMKEYWORD = 'kuku';

function createSubscription(subscriptionParams) {
  return new Promise(function(resolve, reject) {
    var options = {
      method: 'POST',
      url: CREATESUBSCRIPTIONURL,
      form: subscriptionParams,
      headers: {
        apikey: atCredentials.apiKey,
        Accept: 'application/json'
      }
    };

    request(options, function (error, response, body) {
      if (error) return reject(error);
      try {
        var data = JSON.parse(body);
        return resolve(data);
      } catch(err) {
        return reject(err);
      }
    });
  });
}

router.post('/', function(req, res) {
  console.log('Creating subscription...');

  var subscriptionData = {
    username: atCredentials.username,
    phoneNumber: req.body.phoneNumber.replace('0', '+254'),
    checkoutToken: req.body.checkoutToken,
    shortCode: PREMIUMSHORTCODE,
    keyword: PREMIUMKEYWORD,
  }

  console.log(JSON.stringify(subscriptionData, 0, 4));

  return createSubscription(subscriptionData)
  .then(function(data) {
    console.log(JSON.stringify(data, 0, 4));
    if (data.status === 'Success') return res.status(200).json({created: true});
    return res.status(200).json({created: false});
  })
  .catch(function(err) {
    console.log(err);
    return res.status(500).json({
      message: 'Something blew up!'
    });
  });
});

router.post('/new_subscription', function(req, res) {
  console.log('New subscription...');

  console.log(JSON.stringify(req.body, 0, 4));

  res.sendStatus(200);
});

module.exports = router;
